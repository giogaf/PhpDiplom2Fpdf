<?php
require_once '../vendor/setasign/fpdf/fpdf.php';

class DiplomPdf extends FPDF{
	public $verBordes=0;
	public function __construct($pos, $unit,$type){
		parent::__construct($pos,$unit,$type);
		$this->AddPage();
		$this->SetFont('Arial','B',16);
		$this->Image('img/marco1.png',10,-2,-160,-145);
	
	}
		
	public function renderLogo($logo,$x,$y,$res=''){
		$this->Image($logo,$x,$y,$res);
	}
	
	public function renderInstitucion($insti){
	
		$this->MultiCell(0,50,'',$this->verBordes);
		$this->SetFont('Arial','B',24);
		$this->MultiCell(0,10,utf8_decode($insti[0]),$this->verBordes,'C');
		$this->SetFont('Arial','I',10);
		$this->MultiCell(0,10,utf8_decode($insti[1]),$this->verBordes,'C');
	}	
	public function renderCampo($objeto,$etiquetas){
	
		$this->MultiCell(0,5,'',$this->verBordes);
		$this->SetFont('Arial','BI',12);
		$this->MultiCell(0,10,utf8_decode($etiquetas[0]),$this->verBordes,'C');
		$this->SetFont('Arial','B',22);
		$this->MultiCell(0,10,utf8_decode($objeto[0]),$this->verBordes,'C');
		$this->SetFont('Arial','I',12);
		$this->MultiCell(0,10,utf8_decode($etiquetas[1].' '.$objeto[1]),$this->verBordes,'C');
	}

	public function renderAutoridad($objeto,$etiquetas){
	
		$this->Ln();
		$this->sangriaAutoridad();
		$this->SetFont('Arial','I',10);
		$this->Cell(30,10,utf8_decode($etiquetas[0]),$this->verBordes,1);
		$this->sangriaAutoridad();
		$this->SetFont('Arial','B',16);
		$this->Cell(30,10,utf8_decode($objeto[0]),$this->verBordes,1);
		$this->sangriaAutoridad();
		$this->SetFont('Arial','I',12);
		$this->Cell(30,5,utf8_decode($objeto[1]),$this->verBordes);
	}	

	public function renderSeguridad($text){

		$this->SetFont('Arial','I',9);
		$this->Cell(30,9,utf8_decode($text),$this->verBordes,1,'J');

	}
	
	private function sangriaAutoridad(){
		$this->Cell(25,5,'',$this->verBordes);
	}
	
}
