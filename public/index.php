<?php
require_once '../app/DiplomPdf.php';

$institución=['Instituto de Capacitación Laboral Bruce Wayne ','La noche es más oscura antes del amanecer'];
$persona=['Bruno Díaz ','666-666-666'];
$etiquetasPersona=['Hace constar','Identificado con documento:'];
$certificacion=["Mecanica Avanzada de Batimóviles"];
$etiquetaCertificacion=['Curso y Aprobó', 'finalizado el veintiocho (28) de Diciembre de mil novecientos ochenta y uno (1981) en Ciudad Gótica'];
$autoridad=['Joker Villamizar G.','Director'];
$etiquetaAutoridad=['En constancia firma',''];
$txtSeguridad="La autenticidad de este documento puede ser verificado en certi.joker.com.got con el número de serie 666-666-666";
$diplomPdf=new DiplomPdf('L','mm','A4');
$diplomPdf->renderLogo('img/escudo.jpg',120,20);
$diplomPdf->renderInstitucion($institución);
$diplomPdf->renderCampo($persona,$etiquetasPersona);
$diplomPdf->renderCampo($certificacion,$etiquetaCertificacion);
$diplomPdf->renderAutoridad($autoridad, $etiquetaAutoridad);
$diplomPdf->renderLogo('img/escudoOrg.png',122,150,-200);
$diplomPdf->renderSeguridad($txtSeguridad);
$diplomPdf->Output();
//echo 'hula Fpdf';
